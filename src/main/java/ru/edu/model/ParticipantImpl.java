package ru.edu.model;

import java.util.Objects;

/**
 * Информация об участнике
 */
public class ParticipantImpl implements Participant {

    /**
     * Регистрационный номер участника.
     */
    public final Long id;

    /**
     * Участник.
     */
    private Athlete athlete;

    /**
     * Счет участника.
     */
    private long score;

    public ParticipantImpl(long id, Athlete athlete) {
        this.id = id;
        this.athlete = athlete;
    }

    /**
     * Получение информации о регистрационном номере.
     *
     * @return регистрационный номер
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Информация о спортсмене.
     *
     * @return объект спортсмена
     */
    @Override
    public Athlete getAthlete() {
        return athlete;
    }

    /**
     * Счет участника.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    /**
     * Изменение счета участника.
     *
     * @param value - баллы, влияющие на счет.
     */
    @Override
    public void incrementScore(long value) {
        score += value;
    }

    /**
     * Обновление счета участника.
     */
    @Override
    public void updateScore(long value) {
        score = value;
    }

    /**
     * Клонирование объекта участника.
     *
     * @return новый объект с заданными параметрами
     */
    @Override
    public Participant getClone() {
        return new ParticipantImpl(this.id, this.athlete);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ParticipantImpl))
            return false;

        ParticipantImpl that = (ParticipantImpl) o;
        return getId().equals(that.getId()) &&
                getScore() == that.getScore() &&
                Objects.equals(getAthlete(), that.getAthlete());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAthlete(), getScore());
    }

    @Override
    public String toString() {
        return "ParticipantImpl{" +
                "id = " + id +
                ", athlete = " + athlete +
                ", score = " + score +
                '}';
    }
}
