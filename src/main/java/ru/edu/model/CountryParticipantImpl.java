package ru.edu.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Информация о стране-участнике
 */
public class CountryParticipantImpl implements CountryParticipant {

    /**
     * Название страны.
     */
    private String name;

    /**
     * Счет страны.
     */
    private long score;

    /**
     * Список участников от страны.
     */
    private List<Participant> participantList = new ArrayList<>();

    /**
     * @param name
     */
    public CountryParticipantImpl(String name) {
        this.name = name;
    }

    /**
     * Установка счета стране.
     *
     * @param score
     */
    public void setScore(long score) {
        this.score = score;
    }

    /**
     * Название страны.
     *
     * @return название
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Список участников от страны.
     *
     * @return список участников
     */
    @Override
    public List<Participant> getParticipants() {
        return participantList;
    }

    /**
     * Счет страны.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    /**
     * Добавление участника в список участников его страны.
     *
     * @param participant - участник
     */
    public void addParticipant(Participant participant) {
        if (name != participant.getAthlete().getCountry()) {
            throw new IllegalArgumentException("The athlete belongs to another country.");
        }
        participantList.add(participant);
    }

    /**
     * Вывод на печать информации о стране, участвующей в соревнованиях.
     *
     * @return печать
     */
    @Override
    public String toString() {
        return "CountryParticipantImpl {" +
                "name = '" + name + '\'' +
                ", score = " + score +
                '}';
    }
}
