package ru.edu.model;

/**
 * Информация об атлете.
 */

public class AthleteImpl implements Athlete {

    private String firstName;
    private String lastName;
    private String country;

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    /**
     * Сравнение объектов.
     */
    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AthleteImpl other = (AthleteImpl) o;
        if (firstName != other.firstName)
            return false;
        if (lastName != other.lastName)
            return false;
        if (country != other.country)
            return false;
        return true;
    }

    /**
     * Возврат хэшкода объекта.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + firstName.length();
        result = prime * result + lastName.length();
        result = prime * result + country.length();

        return result;
    }

    /**
     * Вывод на печать информации об объекте.
     *
     * @return печать
     */
    @Override
    public String toString() {
        return "FirstName = " + firstName + ' ' + "LastName = "
                + lastName + ' ' + "(Country = " + country + ")";
    }

    public static class Builder {

        private AthleteImpl athlete = new AthleteImpl();

        public AthleteImpl build() {
            return athlete;
        }

        /**
         * Установка параметра firstName.
         *
         * @return объект
         */
        public Builder setFirstName(String input) {
            athlete.firstName = input;
            return this;
        }

        /**
         * Установка параметра lastName.
         *
         * @return объект
         */
        public Builder setLastName(String input) {
            athlete.lastName = input;
            return this;
        }

        /**
         * Установка параметра country.
         *
         * @return объект
         */
        public Builder setCountry(String input) {
            athlete.country = input;
            return this;
        }
    }
}
