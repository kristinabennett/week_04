package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.Competition;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class CountryParticipantImplTest {

    private CountryParticipantImpl countryParticipant1;
    private CountryParticipantImpl countryParticipant2;

    private AthleteImpl athlete1;
    private AthleteImpl athlete2;
    private AthleteImpl athlete3;

    private Participant participant1;
    private Participant participant2;
    private Participant participant3;

    @Mock
    private Competition testCompetition;


    @Before
    public void setUp() {
        openMocks(this);

        countryParticipant1 = new CountryParticipantImpl("SomeCountry");
        countryParticipant2 = new CountryParticipantImpl("AnotherCountry");

        athlete1 = new AthleteImpl.Builder()
                .setFirstName("SomeFirstName")
                .setLastName("SomeLastName")
                .setCountry("SomeCountry")
                .build();

        athlete2 = new AthleteImpl.Builder()
                .setFirstName("AnotherFirstName")
                .setLastName("AnotherLastName")
                .setCountry("AnotherCountry")
                .build();

        athlete3 = new AthleteImpl.Builder()
                .setFirstName("DifferentFirstName")
                .setLastName("DifferentLastName")
                .setCountry("DifferentCountry")
                .build();

        participant1 = new ParticipantImpl(1, athlete1);
        participant2 = new ParticipantImpl(2, athlete2);
        participant3 = new ParticipantImpl(3, athlete3);

        testCompetition = mock(Competition.class);
    }

    @Test
    public void testSetScore() {
        countryParticipant1.setScore(100);
        countryParticipant2.setScore(101);

        assertEquals(100, countryParticipant1.getScore());
        assertEquals(101, countryParticipant2.getScore());
    }

    @Test
    public void testGetName() {
        assertEquals("SomeCountry", countryParticipant1.getName());
        assertEquals("AnotherCountry", countryParticipant2.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCountryParticipants() {
        when(testCompetition.register(athlete1)).thenReturn(participant1);
        when(testCompetition.register(athlete2)).thenReturn(participant2);
        when(testCompetition.register(athlete3)).thenReturn(participant3);

        countryParticipant1.addParticipant(participant1);
        countryParticipant1.addParticipant(participant2);

        List<Participant> participantList = countryParticipant1.getParticipants();

        assertEquals(1, participantList.size());
    }

    @Test
    public void testGetParticipants() {
        when(testCompetition.register(athlete1)).thenReturn(participant1);
        when(testCompetition.register(athlete2)).thenReturn(participant2);
        when(testCompetition.register(athlete3)).thenReturn(participant3);

        countryParticipant1.addParticipant(participant1);
        countryParticipant2.addParticipant(participant2);

        List<Participant> participantList = countryParticipant1.getParticipants();

        assertEquals(1, participantList.size());
    }

    @Test
    public void testGetScore() {
        countryParticipant1.setScore(100);
        countryParticipant2.setScore(101);

        assertEquals(100, countryParticipant1.getScore());
        assertEquals(101, countryParticipant2.getScore());
    }

    @Test
    public void testToString() {
        assertEquals("CountryParticipantImpl {name = 'SomeCountry'" +
                        ", score = 0" + '}',
                countryParticipant1.toString());

        assertEquals("CountryParticipantImpl {name = 'AnotherCountry'" +
                        ", score = 0" + '}',
                countryParticipant2.toString());

    }
}