package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ParticipantImplTest {

    private AthleteImpl athlete1;
    private AthleteImpl athlete2;
    private ParticipantImpl participant1;
    private ParticipantImpl participant2;

    @Before
    public void setUp() {
        athlete1 = new AthleteImpl.Builder()
                .setFirstName("SomeFirstName")
                .setLastName("SomeLastName")
                .setCountry("SomeCountry")
                .build();

        athlete2 = new AthleteImpl.Builder()
                .setFirstName("AnotherFirstName")
                .setLastName("AnotherLastName")
                .setCountry("AnotherCountry")
                .build();

        participant1 = new ParticipantImpl(1, athlete1);
        participant2 = new ParticipantImpl(2, athlete2);
    }

    @Test
    public void getId() {
        Participant mock = mock(Participant.class);
        when(mock.getId()).thenReturn(participant1.id);
    }

    @Test
    public void getAthlete() {
        assertEquals(athlete1, participant1.getAthlete());
        assertEquals(athlete2, participant2.getAthlete());
    }

    @Test
    public void getScore() {
        participant1.updateScore(50);
        participant2.updateScore(40);
        assertEquals(50, participant1.getScore());
        assertEquals(40, participant2.getScore());
    }

    @Test
    public void incrementScore() {
        participant1.updateScore(50);
        assertEquals(50, participant1.getScore());
        participant1.incrementScore(50);
        assertEquals(100, participant1.getScore());
    }

    @Test
    public void updateScore() {
        participant1.updateScore(50);
        participant2.updateScore(40);
        assertEquals(50, participant1.getScore());
        assertEquals(40, participant2.getScore());
    }

    @Test
    public void getClone() {
        assertTrue(participant1.equals(participant1.getClone()));
    }

    @Test
    public void testGetClone() {
        assertTrue(participant2.equals(participant2.getClone()));
    }

    @Test
    public void testEquals() {
        assertTrue(participant1.equals(participant1));
        assertTrue(participant1.equals(participant1.getClone()));
        assertFalse(participant1.equals("notparticipant"));
        assertFalse(participant1.equals(null));
    }

    @Test
    public void testToString() {
        assertEquals("ParticipantImpl{id = 1" +
                        ", athlete = " + athlete1.toString() +
                        ", score = 0" + '}',
                participant1.toString());

        assertEquals("ParticipantImpl{id = 2" +
                        ", athlete = " + athlete2.toString() +
                        ", score = 0" + '}',
                participant2.toString());

    }
}