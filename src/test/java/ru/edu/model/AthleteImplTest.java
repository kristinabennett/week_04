package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AthleteImplTest {

    private AthleteImpl athlete1;
    private AthleteImpl athlete2;

    @Before
    public void setUp() {

        athlete1 = new AthleteImpl.Builder()
                .setFirstName("SomeFirstName")
                .setLastName("SomeLastName")
                .setCountry("SomeCountry")
                .build();

        athlete2 = new AthleteImpl.Builder()
                .setFirstName("AnotherFirstName")
                .setLastName("AnotherLastName")
                .setCountry("AnotherCountry")
                .build();
    }

    @Test
    public void builderWithoutName() {
        AthleteImpl.builder()
                .setCountry("SomeCountry")
                .build();
    }

    @Test
    public void builderWithoutCountry() {
        AthleteImpl.builder()
                .setFirstName("SomeFirstName")
                .setLastName("SomeLastName")
                .build();
    }

    @Test
    public void testBuilder() {
        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("DifferentFirstName")
                .setLastName("DifferentLastName")
                .setCountry("DifferentCountry")
                .build();
    }

    @Test
    public void testGetFirstName() {
        testBuilder();
        assertEquals("SomeFirstName", athlete1.getFirstName());
        assertEquals("AnotherFirstName", athlete2.getFirstName());
    }

    @Test
    public void testGetLastName() {
        assertEquals("SomeLastName", athlete1.getLastName());
        assertEquals("AnotherLastName", athlete2.getLastName());
    }

    @Test
    public void testGetCountry() {
        assertEquals("SomeCountry", athlete1.getCountry());
        assertEquals("AnotherCountry", athlete2.getCountry());
    }

    @Test
    public void testEquals() {
        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("SomeFirstName")
                .setLastName("SomeLastName")
                .setCountry("SomeCountry")
                .build();
        assertTrue(athlete1.equals(athlete3));
    }

    @Test
    public void testEqualsThisObj() {
        assertTrue(athlete1.equals(athlete1));
    }

    @Test
    public void testEqualsOtherObj() {
        String athlete3 = "athlete3";
        assertFalse(athlete1.equals(athlete3));
    }

    @Test
    public void testEqualsFirstName() {
        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("IncorrectSomeFirstName")
                .setLastName("SomeLastName")
                .setCountry("SomeCountry")
                .build();
        assertFalse(athlete1.equals(athlete3));
    }

    @Test
    public void testEqualsLastName() {
        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("SomeFirstName")
                .setLastName("IncorrectSomeLastName")
                .setCountry("SomeCountry")
                .build();
        assertFalse(athlete1.equals(athlete3));
    }

    @Test
    public void testEqualsCountry() {
        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("SomeFirstName")
                .setLastName("SomeLastName")
                .setCountry("IncorrectSomeCountry")
                .build();
        assertFalse(athlete1.equals(athlete3));
    }

    @Test
    public void testEqualsNull() {

        assertFalse(athlete1.equals(null));
    }

    @Test
    public void testHashCode() {
        AthleteImpl athlete3 = new AthleteImpl.Builder()
                .setFirstName("SomeFirstName")
                .setLastName("SomeLastName")
                .setCountry("SomeCountry")
                .build();
        assertEquals(athlete1.hashCode(), athlete3.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("FirstName = SomeFirstName " +
                        "LastName = SomeLastName (Country = SomeCountry)",
                athlete1.toString());

        assertEquals("FirstName = AnotherFirstName " +
                        "LastName = AnotherLastName (Country = AnotherCountry)",
                athlete2.toString());
    }
}