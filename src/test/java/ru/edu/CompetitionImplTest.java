package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.AthleteImpl;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

public class CompetitionImplTest {

    private AthleteImpl athlete1;
    private AthleteImpl athlete2;
    private AthleteImpl athlete3;
    private AthleteImpl athlete4;
    private Participant participant1;
    private Participant participant2;
    private Participant participant3;
    private CompetitionImpl testCompetition;

    @Before
    public void setUp() {

        athlete1 = new AthleteImpl.Builder()
                .setFirstName("SomeFirstName")
                .setLastName("SomeLastName")
                .setCountry("SomeCountry")
                .build();

        athlete2 = new AthleteImpl.Builder()
                .setFirstName("AnotherFirstName")
                .setLastName("AnotherLastName")
                .setCountry("AnotherCountry")
                .build();

        athlete3 = new AthleteImpl.Builder()
                .setFirstName("DifferentFirstName")
                .setLastName("DifferentLastName")
                .setCountry("DifferentCountry")
                .build();

        testCompetition = new CompetitionImpl();

        participant1 = testCompetition.register(athlete1);
        participant2 = testCompetition.register(athlete2);
        participant3 = testCompetition.register(athlete3);
    }

    @Test
    public void testRegister() {
        athlete4 = new AthleteImpl.Builder()
                .setFirstName("TestFirstName")
                .setLastName("TestLastName")
                .setCountry("TestCountry")
                .build();

        testCompetition.register(athlete4);

        assertEquals(4, testCompetition.getParticipantMapSize());
    }

    @Test
    public void testRegisterDuplicate() {
        boolean success = false;
        try {
            testCompetition.register(athlete1);
        } catch (IllegalStateException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testUpdateScoreId() {
        assertEquals(0, participant1.getScore());
        assertEquals(0, participant2.getScore());

        testCompetition.updateScore(participant1.getId(), 10);
        testCompetition.updateScore(participant2.getId(), 15);

        participant1 = testCompetition.getParticipant(participant1.getId());
        participant2 = testCompetition.getParticipant(participant2.getId());

        assertEquals(10, participant1.getScore());
        assertEquals(15, participant2.getScore());
    }

    @Test
    public void testUpdateScoreIdNull() {
        boolean success = false;
        try {
            testCompetition.updateScore(99, 10);
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testUpdateScoreParticipant() {
        assertEquals(0, participant1.getScore());
        assertEquals(0, participant2.getScore());

        testCompetition.updateScore(participant1, 10);
        testCompetition.updateScore(participant2, 15);

        participant1 = testCompetition.getParticipant(participant1.getId());
        participant2 = testCompetition.getParticipant(participant2.getId());

        assertEquals(10, participant1.getScore());
        assertEquals(15, participant2.getScore());
    }

    @Test
    public void testGetResults() {
        testCompetition.updateScore(participant1.getId(), 10);
        testCompetition.updateScore(participant2.getId(), 15);

        List<Participant> participantsResult = testCompetition.getResults();

        assertEquals(15, participantsResult.get(0).getScore());
        assertEquals("AnotherFirstName", participantsResult.get(0).getAthlete().getFirstName());
        assertEquals(10, participantsResult.get(1).getScore());
        assertEquals("SomeFirstName", participantsResult.get(1).getAthlete().getFirstName());
    }

    @Test
    public void testGetParticipantsCountries() {
        testCompetition.updateScore(participant1.getId(), 10);
        testCompetition.updateScore(participant2.getId(), 15);
        testCompetition.updateScore(participant3.getId(), 40);


        List<CountryParticipant> participantsCountries = testCompetition.getParticipantsCountries();

        assertEquals(40, participantsCountries.get(0).getScore());
        assertEquals("DifferentCountry", participantsCountries.get(0).getName());
        assertEquals(1, participantsCountries.get(0).getParticipants().size());

        assertEquals(15, participantsCountries.get(1).getScore());
        assertEquals("AnotherCountry", participantsCountries.get(1).getName());
        assertEquals(1, participantsCountries.get(1).getParticipants().size());
    }
}